package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("124"),is(new Student("124","B","temp2",3.00)));
        assertThat(studentService.findStudentById("125"),is(new Student("125","C","temp3",2.88)));
        assertThat(studentService.findStudentById("126"),is(new Student("126","D","temp4",1.96)));
        assertThat(studentService.findStudentById("127"),is(new Student("127","E","temp5",1.55)));


    }

    @Test
    public void testFindByPartOfId(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp2",3.00));
        mockStudents.add(new Student("125","C","temp3",2.88));
        mockStudents.add(new Student("126","D","temp4",1.96));
        mockStudents.add(new Student("127","E","temp5",1.55));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("23"),hasItem(new Student("123","A","temp",2.33)));


    }
    @Test
    public void testFindByPartOfId2(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents2 = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents2.add(new Student("221","F","temp6",2.00));
        mockStudents2.add(new Student("231","G","temp7",3.00));
        mockStudents2.add(new Student("241","H","temp8",1.50));
        mockStudents2.add(new Student("251","I","temp9",4.00));
        mockStudents2.add(new Student("261","J","temp0",2.00));
        when(studentDao.findAll()).thenReturn(mockStudents2);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("221","F","temp6",2.00)));


    }



    @Test
    public void testGetAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.3440000000000003));
    }

    @Test
    public void testWithMock(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);

    }

    @Test(expected = NoDataExeption.class)
    public void testNoDataExeption(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }
    @Test(expected = Exeption.class)
    public void testFindByPartOfIdWhenSizeOfOutputArrayIsZero(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("55"),hasSize(0));

    }
    @Test(expected = ArithmeticException.class)
    public void testGetAverageGpaWhenDataIsDivideByZero(){
        List<Student> mockStudents = new ArrayList<>();

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());
    }


}

