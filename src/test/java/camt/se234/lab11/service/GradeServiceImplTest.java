package camt.se234.lab11.service;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(JUnitParamsRunner.class)

public class GradeServiceImplTest {

    @Test
    public void testGetGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(100),is("A"));
        assertThat(gradeService.getGrade(80),is("A"));
        assertThat(gradeService.getGrade(78.9),is("B"));
        assertThat(gradeService.getGrade(75),is("B"));
        assertThat(gradeService.getGrade(74.4),is("C"));
        assertThat(gradeService.getGrade(60),is("C"));
        assertThat(gradeService.getGrade(59.4),is("D"));
        assertThat(gradeService.getGrade(33),is("D"));
        assertThat(gradeService.getGrade(32),is("F"));
        assertThat(gradeService.getGrade(0),is("F"));
    }

    public Object paramsForTestGetGradeParams(){
        return new Object[][]{
                {100,"A"},
                {77,"B"},
                {65,"C"},
                {55,"D"},
                {30,"F"}
        };
    }


    @Test
    @Parameters(method = "paramsForTestGetGradeParams")
    @TestCaseName("Test get Grade Params [{index}] : input is {0}, expect \"{1}\"")
    public void testGetGradeparams(double score, String expectedGrade) {
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(score), is(expectedGrade));
    }

    public Object paramsForTestGetGradeTotalGradeParams(){
        return new Object[][]{
                {40,40,"A"},
                {35,40,"B"},
                {25,37,"C"},
                {25,30,"D"},
                {17,15,"F"}
        };
    }


    @Test
    @Parameters(method = "paramsForTestGetGradeTotalGradeParams")
    @TestCaseName("Test get Total Grade Params [{index}] : input is {0}, {1}, expect \"{2}\"")
    public void testGetTotalGradeparams(double midtermScore, double finalScore, String expectedGrade) {
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getTotalGrade(midtermScore,finalScore),is(expectedGrade));
    }





   


}
