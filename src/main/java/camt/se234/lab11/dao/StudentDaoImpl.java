package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    List<Student> students2;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("124","B","temp2",3.00));
        students.add(new Student("125","C","temp3",2.88));
        students.add(new Student("126","D","temp4",1.96));
        students.add(new Student("127","E","temp5",1.55));

        students2 = new ArrayList<>();
        students2.add(new Student("221","F","temp6",2.00));
        students2.add(new Student("231","G","temp7",3.00));
        students2.add(new Student("241","H","temp8",1.50));
        students2.add(new Student("251","I","temp9",4.00));
        students2.add(new Student("261","J","temp0",2.00));


    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
