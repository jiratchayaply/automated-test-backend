package camt.se234.lab11.service;



public interface GradeService {
    String getGrade(double score);
    String getTotalGrade(double midtermScore,double finalScore);
}
